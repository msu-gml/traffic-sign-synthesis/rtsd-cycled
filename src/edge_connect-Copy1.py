import os
import numpy as np
import torch
from torch.utils.data import DataLoader
from .dataset import Dataset
from .models import InpaintingModel
from .utils import Progbar, create_dir, stitch_images
from .metrics import PSNR, EdgeAccuracy


from skimage.io import imread, imsave
from skimage.transform import resize

def get_item_by_bbox(bbox_info, scene_img, inpainted_crop, sign, sign_mask, context_frac=0.5):
    max_side = max(bbox_info['h'], bbox_info['w'])
    context_size = int(max_side * context_frac) + 1
    context_size_y = context_size + (max_side - bbox_info['h'] + 1) // 2
    context_size_x = context_size + (max_side - bbox_info['w'] + 1) // 2

    scene_min_y = max(0, bbox_info['y'] - context_size_y)
    scene_max_y = min(scene_img.shape[0], bbox_info['y'] + bbox_info['h'] + context_size_y)
    scene_min_x = max(0, bbox_info['x'] - context_size_x)
    scene_max_x = min(scene_img.shape[1], bbox_info['x'] + bbox_info['w'] + context_size_x)

    paste_from_y = context_size_y - (bbox_info['y'] - scene_min_y)
    paste_to_y = scene_max_y - scene_min_y + paste_from_y
    paste_from_x = context_size_x - (bbox_info['x'] - scene_min_x)
    paste_to_x = scene_max_x - scene_min_x + paste_from_x

    sign_y_from = inpainted_crop.shape[0] // 4
    sign_h_fake = sign.shape[0]
    sign_x_from = inpainted_crop.shape[1] // 4
    sign_w_fake = sign.shape[1]
    inpainted_signed = np.zeros((inpainted_crop.shape[0], inpainted_crop.shape[1], 4))
    inpainted_signed[:,:,:3] = inpainted_crop
    
    
    from skimage.filters import gaussian
    #sign_mask = gaussian(sign_mask, 2)
    inpainted_signed[sign_y_from:sign_y_from + sign_h_fake, sign_x_from:sign_x_from + sign_w_fake, :3] = sign * sign_mask + inpainted_signed[sign_y_from:sign_y_from + sign_h_fake, sign_x_from:sign_x_from + sign_w_fake, :3] * (1.0 - sign_mask)
    inpainted_signed[sign_y_from:sign_y_from + sign_h_fake, sign_x_from:sign_x_from + sign_w_fake, 3] = sign_mask[:,:,0]

    inpainted_crop = resize(inpainted_crop, (bbox_info['h'] + context_size_y * 2, bbox_info['w'] + context_size_x * 2), mode='constant', anti_aliasing=True).astype(np.float32)
    inpainted_signed = resize(inpainted_signed, (bbox_info['h'] + context_size_y * 2, bbox_info['w'] + context_size_x * 2), mode='constant', anti_aliasing=True).astype(np.float32)
    
    crop_mask_1 = np.zeros((scene_img.shape[0], scene_img.shape[1], 1))
    crop_mask_1[bbox_info['y'] + 2:bbox_info['y'] + bbox_info['h'] - 4, bbox_info['x'] + 2:bbox_info['x'] + bbox_info['w'] - 4] = 1
    crop_mask_1 = gaussian(crop_mask_1, 2)
    crop_mask_1 = crop_mask_1[bbox_info['y']:bbox_info['y'] + bbox_info['h'], bbox_info['x']:bbox_info['x'] + bbox_info['w']]
    
    scene_img[bbox_info['y']:bbox_info['y'] + bbox_info['h'], bbox_info['x']:bbox_info['x'] + bbox_info['w']] = inpainted_crop[bbox_info['y'] - scene_min_y + paste_from_y:bbox_info['y'] - scene_min_y + paste_from_y + bbox_info['h'], bbox_info['x'] - scene_min_x + paste_from_x:bbox_info['x'] - scene_min_x + paste_from_x + bbox_info['w']] * crop_mask_1 + scene_img[bbox_info['y']:bbox_info['y'] + bbox_info['h'], bbox_info['x']:bbox_info['x'] + bbox_info['w']] * (1 - crop_mask_1)
    
    #scene_img[bbox_info['y']:bbox_info['y'] + bbox_info['h'], bbox_info['x']:bbox_info['x'] + bbox_info['w']] = inpainted_crop[bbox_info['y'] - scene_min_y + paste_from_y:bbox_info['y'] - scene_min_y + paste_from_y + bbox_info['h'], bbox_info['x'] - scene_min_x + paste_from_x:bbox_info['x'] - scene_min_x + paste_from_x + bbox_info['w']]
    
    min_y = np.argwhere(inpainted_signed[:,:,3].sum(axis=1))[0][0] - paste_from_y + scene_min_y
    max_y = np.argwhere(inpainted_signed[:,:,3].sum(axis=1))[-1][0] - paste_from_y + scene_min_y
    min_x = np.argwhere(inpainted_signed[:,:,3].sum(axis=0))[0][0] - paste_from_x + scene_min_x
    max_x = np.argwhere(inpainted_signed[:,:,3].sum(axis=0))[-1][0] - paste_from_x + scene_min_x
    from math import sqrt
    dy = int((max_y - min_y) * (1 - 1 / sqrt(2)) // 4)
    dx = int((max_x - min_x) * (1 - 1 / sqrt(2)) // 4)
    new_bbox = {
        "y" : int(min_y + dy),
        "x" : int(min_x + dx),
        "h" : int(max_y - min_y - 2 * dy),
        "w" : int(max_x - min_x - 2 * dx),
        "ignore" : False,
    }
    
    inpainted_signed[:,:,3] = np.maximum(inpainted_signed[:,:,3], gaussian(inpainted_signed[:,:,3], 1, multichannel=False))

    data_to_sign = (scene_min_y, scene_max_y, scene_min_x, scene_max_x, paste_from_y, paste_to_y, paste_from_x, paste_to_x, inpainted_signed)
    return scene_img, new_bbox, data_to_sign

def add_sign(scene_img, data_to_sign):
    scene_min_y, scene_max_y, scene_min_x, scene_max_x, paste_from_y, paste_to_y, paste_from_x, paste_to_x, inpainted_signed = data_to_sign
    scene_img[scene_min_y:scene_max_y, scene_min_x:scene_max_x] = scene_img[scene_min_y:scene_max_y, scene_min_x:scene_max_x] * (1 - inpainted_signed[paste_from_y:paste_to_y, paste_from_x:paste_to_x,3:]) + inpainted_signed[paste_from_y:paste_to_y, paste_from_x:paste_to_x,:3] * inpainted_signed[paste_from_y:paste_to_y, paste_from_x:paste_to_x,3:]
    return scene_img


class EdgeConnect():
    def __init__(self, config):
        self.config = config

        model_name = 'inpaint'
        
        self.debug = False
        self.model_name = model_name
        self.inpaint_model = InpaintingModel(config).to(config.DEVICE)

        self.psnr = PSNR(255.0).to(config.DEVICE)

        # test mode
        if self.config.MODE == 2:
            self.test_dataset = Dataset(config, training=False)
        else:
            self.train_dataset = Dataset(config, training=True)
            self.val_dataset = Dataset(config, training=True)
            self.sample_iterator = self.val_dataset.create_iterator(config.SAMPLE_SIZE)

        self.samples_path = os.path.join(config.PATH, 'samples')
        self.results_path = os.path.join(config.PATH, 'results')

        if config.RESULTS is not None:
            self.results_path = os.path.join(config.RESULTS)

        if config.DEBUG is not None and config.DEBUG != 0:
            self.debug = True

        self.log_file = os.path.join(config.PATH, 'log_' + model_name + '.dat')

    def load(self):
        self.inpaint_model.load()

    def save(self):
        self.inpaint_model.save()

    def train(self):
        train_loader = DataLoader(
            dataset=self.train_dataset,
            batch_size=self.config.BATCH_SIZE,
            num_workers=8,
            drop_last=True,
            shuffle=True
        )

        epoch = 0
        keep_training = True
        model = self.config.MODEL
        max_iteration = int(float((self.config.MAX_ITERS)))
        total = len(self.train_dataset)

        if total == 0:
            print('No training data was provided! Check \'TRAIN_FLIST\' value in the configuration file.')
            return

        while(keep_training):
            epoch += 1
            print('\n\nTraining epoch: %d' % epoch)

            progbar = Progbar(total, width=20, stateful_metrics=['epoch', 'iter'])

            for items in train_loader:
                self.inpaint_model.train()

                images, images_cropped, masks, icon_img, icon_mask, real_sign_pasted, real_sign_mask, real_icon_img, real_icon_mask = self.cuda(*items)

                # train
                outputs, outputs_with_sign, gen_loss, dis_loss, logs = self.inpaint_model.process(images, images_cropped, masks, icon_img, icon_mask, real_sign_pasted, real_sign_mask, real_icon_img, real_icon_mask)
                outputs_merged = (outputs * masks) + (images_cropped * (1 - masks))

                # metrics
                psnr = self.psnr(self.postprocess(images), self.postprocess(outputs_merged))
                mae = (torch.sum(torch.abs(images - outputs_merged)) / torch.sum(images)).float()
                logs.append(('psnr', psnr.item()))
                logs.append(('mae', mae.item()))
                logs.append(('gen_loss', gen_loss.detach().item()))
                logs.append(('dis_loss', dis_loss.detach().item()))

                for i in range(10):
                    if not isinstance(logs[i][1], float):
                        logs.append((logs[i][0][2:], logs[i][1].detach()))
                    else:
                        logs.append((logs[i][0][2:], logs[i][1]))

                # backward
                self.inpaint_model.backward(gen_loss, dis_loss)
                iteration = self.inpaint_model.iteration


                if iteration >= max_iteration:
                    keep_training = False
                    break

                logs = [
                    ("epoch", epoch),
                    ("iter", iteration),
                ] + logs

                progbar.add(len(images), values=logs if self.config.VERBOSE else [x for x in logs if not x[0].startswith('l_')])

                # log model at checkpoints
                if self.config.LOG_INTERVAL and iteration % self.config.LOG_INTERVAL == 0:
                    self.log(logs)

                # sample model at checkpoints
                if self.config.SAMPLE_INTERVAL and iteration % self.config.SAMPLE_INTERVAL == 0:
                    self.sample()

                # evaluate model at checkpoints
                if self.config.EVAL_INTERVAL and iteration % self.config.EVAL_INTERVAL == 0:
                    print('\nstart eval...\n')
                    self.eval()

                # save model at checkpoints
                if self.config.SAVE_INTERVAL and iteration % self.config.SAVE_INTERVAL == 0:
                    self.save()

        print('\nEnd training....')

    def eval(self):
        val_loader = DataLoader(
            dataset=self.val_dataset,
            batch_size=self.config.BATCH_SIZE,
            drop_last=True,
            shuffle=True
        )

        model = self.config.MODEL
        total = len(self.val_dataset)

        self.inpaint_model.eval()

        progbar = Progbar(total, width=20, stateful_metrics=['it'])
        iteration = 0

        for items in val_loader:
            iteration += 1
            images, images_cropped, masks, icon_img, icon_mask, real_sign_pasted, real_sign_mask, real_icon_img, real_icon_mask = self.cuda(*items)

            outputs, outputs_with_sign, gen_loss, dis_loss, logs = self.inpaint_model.process(images, images_cropped, masks, icon_img, icon_mask, real_sign_pasted, real_sign_mask, real_icon_img, real_icon_mask)
            outputs_merged = (outputs * masks) + (images_cropped * (1 - masks))
            # metrics
            psnr = self.psnr(self.postprocess(images), self.postprocess(outputs_merged))
            mae = (torch.sum(torch.abs(images - outputs_merged)) / torch.sum(images)).float()
            logs.append(('psnr', psnr.item()))
            logs.append(('mae', mae.item()))




            logs = [("it", iteration), ] + logs
            progbar.add(len(images), values=logs)

    def test(self):
        self.test_newdataset()
        return
        self.inpaint_model.eval()

        model = self.config.MODEL
        create_dir(self.results_path)

        test_loader = DataLoader(
            dataset=self.test_dataset,
            batch_size=1,
        )

        index = 0
        for items in test_loader:
            name = self.test_dataset.load_name(index)
            index += 1
            _, images_cropped, masks, icon_img, icon_mask, _, _, _, _ = self.cuda(*items)


            outputs, outputs_with_sign_synt, outputs_with_sign = self.inpaint_model(images_cropped, masks, icon_img, icon_mask)
            outputs_merged = (outputs * masks) + (images_cropped * (1 - masks))

            mask_sign_square = torch.zeros_like(outputs_with_sign)
            y_from = outputs_with_sign.shape[2] // 4
            x_from = outputs_with_sign.shape[2] // 4
            h_from = icon_img.shape[3]
            w_from = icon_img.shape[3]
            mask_sign_square[:,:,y_from:y_from + h_from, x_from:x_from + w_from] = icon_mask
            from skimage.filters import gaussian
            mask_sign_square = mask_sign_square.cpu().numpy()
            mask_sign_square = np.maximum(mask_sign_square, gaussian(mask_sign_square, sigma=1))
            mask_sign_square = torch.tensor(mask_sign_square).cuda()
            outputs_with_sign_merged = (outputs_with_sign * mask_sign_square) + (outputs_merged * (1 - mask_sign_square))
            
            
            output = self.postprocess(outputs_with_sign_merged)[0]
            path = os.path.join(self.results_path, name)
            print(index, name)

            imsave(output, path)

            if self.debug:
                edges = self.postprocess(1 - edges)[0]
                masked = self.postprocess(images_cropped * (1 - masks) + masks)[0]
                fname, fext = name.split('.')

                imsave(edges, os.path.join(self.results_path, fname + '_edge.' + fext))
                imsave(masked, os.path.join(self.results_path, fname + '_masked.' + fext))

        print('\nEnd test....')

    
    def test_newdataset_faster(self):
        self.inpaint_model.eval()

        model = self.config.MODEL
        create_dir(self.results_path)

        test_loader = DataLoader(
            dataset=self.test_dataset,
            batch_size=1,
        )

        fnames_to_idxs = self.test_dataset.road_images.filenames_to_idxs
        cur_fname = 0
        cur_fname_pos = 0
        data_to_signs = []
        cur_bboxes = []
        ans_json = {}
        
        index = 0
        for items in test_loader:
            if cur_fname % 200 == 0:
                import gc
                gc.collect()
            name = self.test_dataset.load_name(index)
            index += 1
            cur_fname_pos += 1
            _, images_cropped, masks, icon_img, icon_mask, _, _, _, _, scene_img = self.cuda(*items[:-1])
            bbox_info = items[-1]
            
            def upd_tensor(tens):
                return np.array(tens.data.cpu())[0].transpose((1,2,0))
            if cur_fname_pos == 1:
                outputs_with_sign_merged = upd_tensor(scene_img)

            outputs, outputs_with_sign_synt, outputs_with_sign = self.inpaint_model(images_cropped, masks, icon_img, icon_mask)
            outputs_merged = (outputs * masks) + (images_cropped * (1 - masks))

            y_from = outputs_with_sign.shape[2] // 4
            x_from = outputs_with_sign.shape[2] // 4
            h_from = icon_img.shape[3]
            w_from = icon_img.shape[3]
            #print(outputs_with_sign_merged.shape, upd_tensor(outputs_merged).shape, upd_tensor(outputs_with_sign[:,:,y_from:y_from + h_from, x_from:x_from + w_from]).shape, upd_tensor(icon_mask).shape)
            outputs_with_sign_merged, new_bbox, data_to_sign = get_item_by_bbox(bbox_info, outputs_with_sign_merged, upd_tensor(outputs_merged), upd_tensor(outputs_with_sign[:,:,y_from:y_from + h_from, x_from:x_from + w_from]), upd_tensor(icon_mask))
            new_bbox['sign_class'] = bbox_info['sign_class']
            cur_bboxes.append(new_bbox)
            data_to_signs.append(data_to_sign)
            
            if cur_fname_pos == len(fnames_to_idxs[cur_fname][1]):
                for data_to_sign in data_to_signs:
                    outputs_with_sign_merged = add_sign(outputs_with_sign_merged, data_to_sign)
                name = "inp_" + fnames_to_idxs[cur_fname][0][:-4] + ".png"

                #output = self.postprocess(outputs_with_sign_merged)[0]
                path = os.path.join(self.results_path, name)
                print(index, name)
                outputs_with_sign_merged = (np.clip(outputs_with_sign_merged, 0, 1.0) * 255).astype(np.uint8)
                imsave(path, outputs_with_sign_merged)
                ans_json[name] = cur_bboxes
                cur_fname_pos = 0
                data_to_signs = []
                cur_bboxes = []
                cur_fname += 1

        with open("new_json.json", "w") as fp:
            import json
            json.dump(ans_json, fp)                
                
        print('\nEnd test create dataset....')    
    
    
    
    
        
    def test_newdataset(self):
        self.inpaint_model.eval()

        model = self.config.MODEL
        create_dir(self.results_path)

        test_loader = DataLoader(
            dataset=self.test_dataset,
            batch_size=1,
        )

        fnames_to_idxs = self.test_dataset.road_images.filenames_to_idxs
        cur_fname = 0
        cur_fname_pos = 0
        data_to_signs = []
        cur_bboxes = []
        ans_json = {}
        
        index = 0
        for items in test_loader:
            if cur_fname % 200 == 0:
                import gc
                gc.collect()
            name = self.test_dataset.load_name(index)
            index += 1
            cur_fname_pos += 1
            _, images_cropped, masks, icon_img, icon_mask, _, _, _, _, scene_img = self.cuda(*items[:-1])
            bbox_info = items[-1]
            
            def upd_tensor(tens):
                return np.array(tens.data.cpu())[0].transpose((1,2,0))
            if cur_fname_pos == 1:
                outputs_with_sign_merged = upd_tensor(scene_img)

            outputs, outputs_with_sign_synt, outputs_with_sign = self.inpaint_model(images_cropped, masks, icon_img, icon_mask)
            outputs_merged = (outputs * masks) + (images_cropped * (1 - masks))

            y_from = outputs_with_sign.shape[2] // 4
            x_from = outputs_with_sign.shape[2] // 4
            h_from = icon_img.shape[3]
            w_from = icon_img.shape[3]
            #print(outputs_with_sign_merged.shape, upd_tensor(outputs_merged).shape, upd_tensor(outputs_with_sign[:,:,y_from:y_from + h_from, x_from:x_from + w_from]).shape, upd_tensor(icon_mask).shape)
            outputs_with_sign_merged, new_bbox, data_to_sign = get_item_by_bbox(bbox_info, outputs_with_sign_merged, upd_tensor(outputs_merged), upd_tensor(outputs_with_sign[:,:,y_from:y_from + h_from, x_from:x_from + w_from]), upd_tensor(icon_mask))
            new_bbox['sign_class'] = bbox_info['sign_class']
            cur_bboxes.append(new_bbox)
            data_to_signs.append(data_to_sign)
            
            if cur_fname_pos == len(fnames_to_idxs[cur_fname][1]):
                for data_to_sign in data_to_signs:
                    outputs_with_sign_merged = add_sign(outputs_with_sign_merged, data_to_sign)
                name = "inp_" + fnames_to_idxs[cur_fname][0][:-4] + ".png"

                #output = self.postprocess(outputs_with_sign_merged)[0]
                path = os.path.join(self.results_path, name)
                print(index, name)
                outputs_with_sign_merged = (np.clip(outputs_with_sign_merged, 0, 1.0) * 255).astype(np.uint8)
                imsave(path, outputs_with_sign_merged)
                ans_json[name] = cur_bboxes
                cur_fname_pos = 0
                data_to_signs = []
                cur_bboxes = []
                cur_fname += 1

        with open("new_json.json", "w") as fp:
            import json
            json.dump(ans_json, fp)                
                
        print('\nEnd test create dataset....')    
        
        
    def sample(self, it=None):
        # do not sample when validation set is empty
        if len(self.val_dataset) == 0:
            return

        self.inpaint_model.eval()

        model = self.config.MODEL
        items = next(self.sample_iterator)
        images, images_cropped, masks, icon_img, icon_mask, real_sign_pasted, real_sign_mask, real_icon_img, real_icon_mask = self.cuda(*items)

        iteration = self.inpaint_model.iteration
        outputs, outputs_with_sign_synt, outputs_with_sign, real_outputs, real_outputs_with_sign_synt, real_outputs_with_sign = self.inpaint_model(images_cropped, masks, icon_img, icon_mask, real_sign_pasted * (1 - real_sign_mask), real_sign_mask, real_icon_img, real_icon_mask)
        outputs_merged = (outputs * masks) + (images_cropped * (1 - masks))
        real_outputs_merged = (real_outputs * real_sign_mask) + (real_sign_pasted * (1 - real_sign_mask))
        
        mask_sign_square = torch.zeros_like(outputs_with_sign)
        y_from = outputs_with_sign.shape[2] // 4
        x_from = outputs_with_sign.shape[2] // 4
        h_from = icon_img.shape[3]
        w_from = icon_img.shape[3]
        mask_sign_square[:,:,y_from:y_from + h_from, x_from:x_from + w_from] = icon_mask
        from skimage.filters import gaussian
        mask_sign_square = mask_sign_square.cpu().numpy()
        mask_sign_square = np.maximum(mask_sign_square, gaussian(mask_sign_square, sigma=1))
        mask_sign_square = torch.tensor(mask_sign_square).cuda()
        outputs_with_sign_merged = (outputs_with_sign * mask_sign_square) + (outputs_merged * (1 - mask_sign_square))

        mask_sign_square = torch.zeros_like(real_outputs_with_sign)
        y_from = real_outputs_with_sign.shape[2] // 4
        x_from = real_outputs_with_sign.shape[2] // 4
        h_from = real_icon_img.shape[3]
        w_from = real_icon_img.shape[3]
        mask_sign_square[:,:,y_from:y_from + h_from, x_from:x_from + w_from] = real_icon_mask
        from skimage.filters import gaussian
        mask_sign_square = mask_sign_square.cpu().numpy()
        mask_sign_square = np.maximum(mask_sign_square, gaussian(mask_sign_square, sigma=1))
        mask_sign_square = torch.tensor(mask_sign_square).cuda()
        real_outputs_with_sign_merged = (real_outputs_with_sign * mask_sign_square) + (real_outputs_merged * (1 - mask_sign_square))
        

        if it is not None:
            iteration = it

        image_per_row = 2
        if self.config.SAMPLE_SIZE <= 6:
            image_per_row = 1

        images = stitch_images(
            self.postprocess(images),
            self.postprocess(images_cropped),
            self.postprocess(outputs),
            self.postprocess(outputs_merged),
            self.postprocess(outputs_with_sign),
            self.postprocess(outputs_with_sign_merged),
            self.postprocess(real_sign_pasted),
            self.postprocess(real_outputs),
            self.postprocess(real_outputs_merged),
            self.postprocess(real_outputs_with_sign),
            self.postprocess(real_outputs_with_sign_synt),
            self.postprocess(real_outputs_with_sign_merged),
            img_per_row = image_per_row
        )


        path = os.path.join(self.samples_path, self.model_name)
        name = os.path.join(path, str(iteration).zfill(5) + ".png")
        create_dir(path)
        print('\nsaving sample ' + name)
        images.save(name)

    def log(self, logs):
        with open(self.log_file, 'a') as f:
            f.write('%s\n' % ' '.join([str(item[1]) for item in logs]))

    def cuda(self, *args):
        return (item.to(self.config.DEVICE) for item in args)

    def postprocess(self, img):
        # [0, 1] => [0, 255]
        img = img * 255.0
        img = img.permute(0, 2, 3, 1)
        return img.int()
