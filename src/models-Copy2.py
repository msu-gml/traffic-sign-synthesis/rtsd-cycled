import os
import torch
import torch.nn as nn
import torch.optim as optim
from .networks import InpaintGenerator, Discriminator, SignsImprovement
from .loss import AdversarialLoss, PerceptualLoss, StyleLoss, calculate_gp


class BaseModel(nn.Module):
    def __init__(self, name, config):
        super(BaseModel, self).__init__()

        self.name = name
        self.config = config
        self.iteration = 0

        self.gen_weights_path = os.path.join(config.PATH, name + '_gen.pth')
        self.dis_weights_path = os.path.join(config.PATH, name + '_dis.pth')

    def load(self):
        if os.path.exists(self.gen_weights_path):
            print('Loading %s generator...' % self.name)

            if torch.cuda.is_available():
                data = torch.load(self.gen_weights_path)
            else:
                data = torch.load(self.gen_weights_path, map_location=lambda storage, loc: storage)

            self.generator.load_state_dict(data['generator'])
            self.sings_improver.load_state_dict(data['sings_improver'])
            self.iteration = data['iteration']

        # load discriminator only when training
        if self.config.MODE == 1 and os.path.exists(self.dis_weights_path):
            print('Loading %s discriminator...' % self.name)

            if torch.cuda.is_available():
                data = torch.load(self.dis_weights_path)
            else:
                data = torch.load(self.dis_weights_path, map_location=lambda storage, loc: storage)

            self.discriminator.load_state_dict(data['discriminator'])
            self.signs_discriminator.load_state_dict(data['signs_discriminator'])

    def save(self):
        print('\nsaving %s...\n' % self.name)
        torch.save({
            'iteration': self.iteration,
            'sings_improver': self.sings_improver.state_dict(),
            'generator': self.generator.state_dict()
        }, self.gen_weights_path)

        torch.save({
            'discriminator': self.discriminator.state_dict(),
            'signs_discriminator': self.signs_discriminator.state_dict()
        }, self.dis_weights_path)


class InpaintingModel(BaseModel):
    def __init__(self, config):
        super(InpaintingModel, self).__init__('InpaintingModel', config)

        # generator input: [rgb(3) + edge(1)]
        # discriminator input: [rgb(3)]
        generator = InpaintGenerator()
        sings_improver = SignsImprovement()
        discriminator = Discriminator(in_channels=3, use_sigmoid=config.GAN_LOSS != 'hinge')
        signs_discriminator = Discriminator(in_channels=3, use_sigmoid=config.GAN_LOSS != 'hinge')
        if len(config.GPU) > 1:
            generator = nn.DataParallel(generator, config.GPU)
            sings_improver = nn.DataParallel(sings_improver, config.GPU)
            discriminator = nn.DataParallel(discriminator , config.GPU)
            signs_discriminator = nn.DataParallel(signs_discriminator , config.GPU)

        l1_loss = nn.L1Loss()
        perceptual_loss = PerceptualLoss()
        style_loss = StyleLoss()
        adversarial_loss = AdversarialLoss(type=config.GAN_LOSS)
        self.gan_loss = config.GAN_LOSS

        self.add_module('generator', generator)
        self.add_module('sings_improver', sings_improver)
        self.add_module('discriminator', discriminator)
        self.add_module('signs_discriminator', signs_discriminator)

        self.add_module('l1_loss', l1_loss)
        self.add_module('perceptual_loss', perceptual_loss)
        self.add_module('style_loss', style_loss)
        self.add_module('adversarial_loss', adversarial_loss)

        self.gen_optimizer = optim.Adam(
            params=generator.parameters(),
            lr=float(config.LR),
            betas=(config.BETA1, config.BETA2)
        )
        self.gen_optimizer.add_param_group({'params': sings_improver.parameters()})

        self.dis_optimizer = optim.Adam(
            params=discriminator.parameters(),
            lr=float(config.LR) * float(config.D2G_LR),
            betas=(config.BETA1, config.BETA2)
        )
        self.dis_optimizer.add_param_group({'params': signs_discriminator.parameters()})

    def process(self, images, images_cropped, masks, icon_img, icon_mask, real_sign_pasted, real_sign_mask, real_icon_img, real_icon_mask):
        self.iteration += 1

        # zero optimizers
        self.gen_optimizer.zero_grad()
        self.dis_optimizer.zero_grad()


        # process outputs
        outputs, outputs_with_sign_synt, outputs_with_sign, real_outputs, real_outputs_with_sign_synt, real_outputs_with_sign = self(images_cropped, masks, icon_img, icon_mask, real_sign_pasted * (1.0 - real_sign_mask), real_sign_mask, real_icon_img, real_icon_mask)
        gen_loss = 0
        dis_loss = 0


        # discriminator loss
        dis_input_real = images
        dis_input_fake = outputs.detach()
        dis_input_real_fake = real_outputs.detach()
        dis_real, _ = self.discriminator(dis_input_real)                    # in: [rgb(3)]
        dis_fake, _ = self.discriminator(dis_input_fake)                    # in: [rgb(3)]
        dis_real_fake, _ = self.discriminator(dis_input_real_fake)                    # in: [rgb(3)]
        dis_real_loss = self.adversarial_loss(dis_real, True, True)
        dis_fake_loss = self.adversarial_loss(dis_fake, False, True)
        dis_real_fake_loss = self.adversarial_loss(dis_real_fake, False, True)
        dis_loss += (dis_real_loss * 2.0 + dis_fake_loss + dis_real_fake_loss) / 4
        
        if self.gan_loss == 'wgangp':
            dis_loss += calculate_gp(dis_input_real.shape[0], dis_input_real, dis_input_fake, self.discriminator)


        # generator adversarial loss
        gen_input_fake = outputs
        gen_input_real_fake = real_outputs
        gen_fake, _ = self.discriminator(gen_input_fake)                    # in: [rgb(3)]
        gen_real_fake, _ = self.discriminator(gen_input_real_fake)                    # in: [rgb(3)]
        gen_gan_loss = self.adversarial_loss(gen_fake, True, False) * self.config.INPAINT_ADV_LOSS_WEIGHT
        gen_gan_real_fake_loss = self.adversarial_loss(gen_real_fake, True, False) * self.config.INPAINT_ADV_LOSS_WEIGHT
        gen_loss += (gen_gan_loss + gen_gan_real_fake_loss) / 2


        # generator l1 loss
        gen_l1_loss = self.l1_loss(outputs, images) * self.config.L1_LOSS_WEIGHT / torch.mean(masks) + self.l1_loss(real_outputs * (1.0 - real_sign_mask), real_sign_pasted * (1.0 - real_sign_mask)) * self.config.L1_LOSS_WEIGHT / torch.mean(1.0 - real_sign_mask)
        gen_loss += gen_l1_loss


        # generator perceptual loss
        gen_content_loss = self.perceptual_loss(outputs, images) #+ self.perceptual_loss(real_outputs * (1.0 - real_sign_mask), real_sign_pasted * (1.0 - real_sign_mask))
        gen_content_loss = gen_content_loss * self.config.CONTENT_LOSS_WEIGHT
        gen_loss += gen_content_loss


        # generator style loss
        gen_style_loss = self.style_loss(outputs * masks, images * masks)
        gen_style_loss = gen_style_loss * self.config.STYLE_LOSS_WEIGHT
        gen_loss += gen_style_loss

        
        
        
        
        
        
        
        
        
        # discriminator loss
        dis_input_real = real_sign_pasted
        dis_input_fake = outputs_with_sign.detach()
        dis_real_input_fake = real_outputs_with_sign.detach()
        dis_real, _ = self.signs_discriminator(dis_input_real)                    # in: [rgb(3)]
        dis_fake, _ = self.signs_discriminator(dis_input_fake)                    # in: [rgb(3)]
        dis_real_fake, _ = self.signs_discriminator(dis_real_input_fake)                    # in: [rgb(3)]
        dis_real_loss_sign = self.adversarial_loss(dis_real, True, True)
        dis_fake_loss_sign = self.adversarial_loss(dis_fake, False, True)
        dis_real_fake_loss_sign = self.adversarial_loss(dis_real_fake, False, True)
        dis_loss_sign = (dis_real_loss_sign * 2.0 + dis_fake_loss_sign + dis_real_fake_loss_sign) / 4
        dis_loss += dis_loss_sign

        if self.gan_loss == 'wgangp':
            dis_loss += calculate_gp(dis_input_real.shape[0], dis_input_real, dis_input_fake, self.signs_discriminator)

        
        # generator adversarial loss
        gen_input_fake = outputs_with_sign
        gen_input_real_fake = real_outputs_with_sign
        gen_fake, _ = self.signs_discriminator(gen_input_fake)                    # in: [rgb(3)]
        gen_real_fake, _ = self.signs_discriminator(gen_input_real_fake)                    # in: [rgb(3)]
        gen_gan_loss_sign = self.adversarial_loss(gen_fake, True, False) * self.config.INPAINT_ADV_LOSS_WEIGHT
        gen_gan_real_fake_loss_sign = self.adversarial_loss(gen_real_fake, True, False) * self.config.INPAINT_ADV_LOSS_WEIGHT
        gen_loss += (gen_gan_loss_sign + gen_gan_real_fake_loss_sign) / 2.0


        # generator l1 loss
        mask_sign_square = torch.ones_like(masks)
        y_from = outputs_with_sign.shape[2] // 4
        x_from = outputs_with_sign.shape[2] // 4
        h_from = icon_img.shape[3]
        w_from = icon_img.shape[3]
        mask_sign_square[:,:,y_from:y_from + h_from, x_from:x_from + w_from] = 1 - icon_mask
        gen_l1_loss_sign = self.l1_loss(outputs_with_sign * mask_sign_square, outputs * mask_sign_square) * self.config.L1_LOSS_WEIGHT / torch.mean(mask_sign_square)
        gen_l1_loss_sign += self.l1_loss(real_outputs_with_sign, real_sign_pasted) * self.config.L1_LOSS_WEIGHT
        gen_loss += gen_l1_loss_sign / 2
        
        
        real_mask_sign_square = torch.ones_like(real_sign_mask)
        y_from = real_outputs_with_sign.shape[2] // 4
        x_from = real_outputs_with_sign.shape[2] // 4
        h_from = real_icon_img.shape[3]
        w_from = real_icon_img.shape[3]
        real_mask_sign_square[:,:,y_from:y_from + h_from, x_from:x_from + w_from] = 1 - real_icon_mask


        # generator perceptual loss
        gen_content_loss_sign = 2.0 * self.perceptual_loss(outputs_with_sign * (1 - mask_sign_square), outputs_with_sign_synt * (1 - mask_sign_square)) + self.perceptual_loss(real_outputs_with_sign * real_sign_mask, real_sign_pasted * real_sign_mask) + self.perceptual_loss(real_outputs_with_sign * (1 - real_mask_sign_square), real_outputs_with_sign_synt * (1 - real_mask_sign_square)) * 0.6 # 1.0
        gen_content_loss_sign += 0.2 * self.style_loss(outputs_with_sign, outputs) + 0.2 * self.style_loss(real_outputs_with_sign, real_outputs)
        gen_content_loss_sign = 0.25 * gen_content_loss_sign * self.config.CONTENT_LOSS_WEIGHT
        gen_loss += gen_content_loss_sign


        # generator style loss
        gen_style_loss_sign = self.style_loss(outputs_with_sign, real_sign_pasted) + self.style_loss(real_outputs_with_sign, real_sign_pasted) + 0.3 * self.style_loss(outputs_with_sign, outputs) + 0.3 * self.style_loss(real_outputs_with_sign, real_outputs) # 0.01
        gen_style_loss_sign = 0.01 * gen_style_loss_sign * self.config.STYLE_LOSS_WEIGHT
        gen_loss += gen_style_loss_sign
        
        
        
        

        # create logs
        logs = [
            ("l_d2", dis_loss.item()),
            ("l_g2", gen_gan_loss.item()),
            ("l_l1", gen_l1_loss.item()),
            ("l_per", gen_content_loss.item()),
            ("l_sty", gen_style_loss.item()),

            ("l_d2_sign", dis_loss_sign.item()),
            ("l_g2_sign", gen_gan_loss_sign.item()),
            ("l_l1_sign", gen_l1_loss_sign.item()),
            ("l_per_sign", gen_content_loss_sign.item()),
            ("l_sty_sign", gen_style_loss_sign.item()),
        ]

        return outputs, outputs_with_sign, gen_loss, dis_loss, logs

    def forward(self, images_cropped, masks, icon_img, icon_mask, real_sign_cropped=None, real_sign_mask=None, real_icon_img=None, real_icon_mask=None):
        inputs = torch.cat((images_cropped, masks), dim=1)
        outputs = self.generator(inputs)                                    # in: [rgb(3) + edge(1)]
        
        outputs_with_sign_synt = outputs.clone()
        y_from = outputs_with_sign_synt.shape[2] // 4
        x_from = outputs_with_sign_synt.shape[2] // 4
        h_from = icon_img.shape[3]
        w_from = icon_img.shape[3]
        outputs_with_sign_synt[:,:,y_from:y_from + h_from, x_from:x_from + w_from] = icon_img * icon_mask + outputs_with_sign_synt[:,:,y_from:y_from + h_from, x_from:x_from + w_from] * (1 - icon_mask)
        outputs_with_sign = self.sings_improver(outputs_with_sign_synt)
        if real_sign_cropped is None:
            return outputs, outputs_with_sign_synt, outputs_with_sign
        
        
        real_outputs = self.generator(torch.cat((real_sign_cropped, real_sign_mask), dim=1))
        
        real_outputs_with_sign_synt = real_outputs.clone()
        y_from = real_outputs_with_sign_synt.shape[2] // 4
        x_from = real_outputs_with_sign_synt.shape[2] // 4
        h_from = icon_img.shape[3]
        w_from = icon_img.shape[3]
        real_outputs_with_sign_synt[:,:,y_from:y_from + h_from, x_from:x_from + w_from] = real_icon_img * real_icon_mask + real_outputs_with_sign_synt[:,:,y_from:y_from + h_from, x_from:x_from + w_from] * (1 - real_icon_mask)
        real_outputs_with_sign = self.sings_improver(real_outputs_with_sign_synt)
        
        return outputs, outputs_with_sign_synt, outputs_with_sign, real_outputs, real_outputs_with_sign_synt, real_outputs_with_sign

    def backward(self, gen_loss=None, dis_loss=None):
        dis_loss.backward()
        self.dis_optimizer.step()

        gen_loss.backward()
        self.gen_optimizer.step()
