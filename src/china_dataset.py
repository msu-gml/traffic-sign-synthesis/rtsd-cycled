import os
import glob
import scipy
import torch
import random
import numpy as np
import torchvision.transforms.functional as F
from torch.utils.data import DataLoader
from PIL import Image
from skimage.feature import canny
from skimage.color import rgb2gray, gray2rgb
from .utils import create_mask

import json
import random
from skimage.io import imread
from skimage.transform import resize
from skimage.transform import rotate
import numpy as np
import matplotlib.pyplot as plt
from math import sqrt
from skimage.color import rgb2hsv, hsv2rgb
from skimage.filters import gaussian
from random import randrange, seed, uniform
from scipy.signal import convolve2d

from skimage.color import rgb2hsv, hsv2rgb
from skimage.filters import gaussian
from random import randrange, seed, uniform
from scipy.signal import convolve2d


def conv(img, k):
    res = []
    for i in range(img.shape[-1]):
        res.append(convolve2d(img[..., i], k, mode='same', boundary='symm'))
    return np.dstack(res)


def mul_mask(img, mask):
    res = img.copy()
    res[..., 0] *= mask
    res[..., 1] *= mask
    res[..., 2] *= mask
    return res


def blend_images(icon, bg_img, shift=(0, 0)):
    bg = bg_img.copy()
    mask = icon[..., 3]
    r, c = shift
    n_rows, n_cols, _ = icon.shape
    bg_patch = bg[r:r + n_rows, c:c + n_cols, ...]
    blended_sign = mul_mask(icon[..., :3].astype('float'), mask) + \
        mul_mask(bg_patch.astype('float'), (1 - mask))
    bg[r:r + n_rows, c:c + n_cols] = blended_sign

    full_mask = np.zeros(bg_img.shape[:2], dtype=mask.dtype)
    full_mask[r:r + n_rows, c:c + n_cols] = mask

    return bg.clip(0, 1), full_mask

def change_color(img):
    res = img.copy()
    hsv = rgb2hsv(res[..., :3])

    h_factor = uniform(-0.05, 0.05)
    hsv[...,0] += h_factor
    hsv[..., 0][hsv[..., 0] < 0] += 1
    hsv[..., 0][hsv[..., 0] > 1] -= 1

    s_factor = uniform(0.5, 0.8)
    hsv[..., 1] *= s_factor
    np.clip(hsv[..., 1], 0, 1, out=hsv[..., 1])

    v_factor = uniform(0.5, 0.7)
    hsv[..., 2] *= v_factor
    np.clip(hsv[..., 2], 0, 1, out=hsv[..., 2])

    res[..., :3] = hsv2rgb(hsv)
    return res


def build_motion_kernel(value, angle):
    kernel = np.zeros((value, value))
    kernel[value // 2] = 1
    kernel = rotate(kernel, angle)
    kernel = kernel / kernel.sum()
    return kernel

def augment_icon(sign, size, transform=True, is_test=False, is_distort_sign=True):
    h, w = sign.shape[:2]

    if transform:
        sign = change_color(sign)

    if transform:
        if is_test:
            angle = uniform(-15, 15)
        else:
            angle = uniform(-15, 15)
        sign = rotate(sign, angle)

        value = randrange(1, size // 15 + 1)
        angle = uniform(-90, 90)
        sign = conv(sign, build_motion_kernel(value, angle))

        sigma = 2.5 * (size / 100) / uniform(1, 3)
        if is_distort_sign:
            sign[:,:,:3] = gaussian(sign[:,:,:3], sigma, multichannel=True)
        sign[:,:,3] = gaussian(sign[:,:,3], sigma // 2, multichannel=False)

    return sign


class IconsImages(object):
    def __init__(self, imgs_path):
        self.imgs, self.imgs_filenames, self.classes, self.int_to_classes = self.read_data(imgs_path)

    def get_int_to_classes(self, index):
        cls = self.int_to_classes[index]
        return cls
    
    def get_classes_to_int(self, cls):
        if cls in self.classes.keys():
            return self.classes[cls]
        return -1
    
    def get_length(self):
        self.length = len(self.imgs)
        return self.length
    
    def read_data(self, path):
        import os
        icons = {}
        classes = {}
        int_to_classes = []
        imgs_filenames = {}
        for d in sorted(os.listdir(path)):
            cls_str = '.'.join(d.split('.')[:-1])
            imgs_filenames[cls_str] = os.path.join(path, d)
            icons[cls_str] = imread(imgs_filenames[cls_str]).astype(np.float32)
            classes[cls_str] = len(classes)
            int_to_classes.append(cls_str)
        return icons, imgs_filenames, classes, int_to_classes
    
    def get_item(self, cls):
        img = self.imgs[self.int_to_classes[cls]]
        if img.max() > 2:
            img = img / 255.0
        return img, self.imgs_filenames[self.int_to_classes[cls]]
    
    def get_placed_icon(self, cls, resolution, randgen, is_test=False, is_distort_sign=True):
        img = self.imgs[self.int_to_classes[cls]]
        shape_distort = (int(img.shape[0] * randgen.uniform(0.9, 1.1)), int(img.shape[1] * randgen.uniform(0.9, 1.1)))
        img = resize(img, shape_distort, mode='constant', anti_aliasing=True).astype(np.float32)

        side = int(resolution * 0.7) # 0.9
        if img.shape[0] > img.shape[1]:
            new_icon_shape = (side, int(side * img.shape[1] / img.shape[0]))
        else:
            new_icon_shape = (int(side * img.shape[0] / img.shape[1]), side)
        img = resize(img, new_icon_shape, mode='constant', anti_aliasing=True).astype(np.float32)
        if img.max() > 2:
            img = img / 255.0
        padded = np.zeros((resolution, resolution, 4), dtype=np.float32)
        from_y = (resolution - img.shape[0]) // 2
        from_x = (resolution - img.shape[1]) // 2
        padded[from_y:from_y + img.shape[0], from_x:from_x + img.shape[1]] = img
        padded = augment_icon(padded, resolution, is_test=is_test, is_distort_sign=is_distort_sign)
        padded[:,:,:3] = padded[:,:,:3] * padded[:,:,3:]
        return padded[:,:,:3], padded[:,:,3]


class RtsdRoadImages(object):
    def __init__(self,
                 json_path='/mydata/kursovaya4/BicycleGAN/data_rtsd/train_rtsd.json',
                 rtsd_images_dir='/mydata/kursovaya4/BicycleGAN/data_rtsd/detection_data/',
                 resolution=128,
                 context_frac=0.5,
                 classes=None, training=True):
        self.resolution = resolution
        self.rtsd_images_dir = rtsd_images_dir
        self.context_frac = context_frac
        with open(json_path, 'r') as fp:
            json_data_full = json.load(fp)
        self.json_data = []
        self.filenames_to_idxs = []
        #for fname, lst in json_data_full.items():
        #    self.json_data.extend([(fname, item) for item in lst if (not item['ignore'])])
        if classes is not None:
            classes = set(classes.keys())
        for fname, lst in json_data_full.items():
            len_start = len(self.json_data)
            if training:
                if classes is not None:
                    self.json_data.extend([(fname, item) for item in lst if (not item['ignore']) and ('.'.join(item['sign_class'].split('_')) in classes)])
                else:
                    self.json_data.extend([(fname, item) for item in lst if (not item['ignore'])])
            else:
                self.json_data.extend([(fname, item) for item in lst])
            self.filenames_to_idxs.append((fname, [i for i in range(len_start, len(self.json_data))]))
        print("rtsd size", len(self.json_data))
    
    def get_length(self):
        return len(self.json_data)
    
    def get_item_by_bbox(self, bbox_info, scene_img):
        max_side = max(bbox_info['h'], bbox_info['w'])
        context_size = int(max_side * self.context_frac) + 1
        context_size_y = context_size + (max_side - bbox_info['h'] + 1) // 2
        context_size_x = context_size + (max_side - bbox_info['w'] + 1) // 2
        
        scene_min_y = max(0, bbox_info['y'] - context_size_y)
        scene_max_y = min(scene_img.shape[0], bbox_info['y'] + bbox_info['h'] + context_size_y)
        scene_min_x = max(0, bbox_info['x'] - context_size_x)
        scene_max_x = min(scene_img.shape[1], bbox_info['x'] + bbox_info['w'] + context_size_x)
                
        paste_from_y = context_size_y - (bbox_info['y'] - scene_min_y)
        paste_to_y = scene_max_y - scene_min_y + paste_from_y
        paste_from_x = context_size_x - (bbox_info['x'] - scene_min_x)
        paste_to_x = scene_max_x - scene_min_x + paste_from_x
        
        to_paste_img = np.zeros((bbox_info['h'] + context_size_y * 2, bbox_info['w'] + context_size_x * 2, 3), dtype=np.float32)
        to_paste_img[paste_from_y:paste_to_y, paste_from_x:paste_to_x] = scene_img[scene_min_y:scene_max_y, scene_min_x:scene_max_x]
        if to_paste_img.max() > 2.0:
            to_paste_img = to_paste_img / 255.0
        pasted_mask = np.zeros((bbox_info['h'] + context_size_y * 2, bbox_info['w'] + context_size_x * 2), dtype=np.float32)
        pasted_mask[context_size_y:context_size_y + bbox_info['h'], context_size_x:context_size_x + bbox_info['w']] = 1.0

        sign_with_area = resize(to_paste_img, (self.resolution, self.resolution), mode='constant', anti_aliasing=True).astype(np.float32)
        sign_mask = resize(pasted_mask, (self.resolution, self.resolution), mode='constant', anti_aliasing=True)
        sign_mask = (sign_mask > 0).astype(np.float32)
        
        new_bbox_info = bbox_info.copy()
        vsp = np.argwhere(sign_mask.sum(axis=1))
        new_bbox_info['y'] = vsp[0][0]
        new_bbox_info['h'] = vsp[-1][0] - vsp[0][0] + 1
        vsp = np.argwhere(sign_mask.sum(axis=0))
        new_bbox_info['x'] = vsp[0][0]
        new_bbox_info['w'] = vsp[-1][0] - vsp[0][0] + 1

        return sign_with_area, sign_mask, bbox_info['sign_class'], new_bbox_info
    
    def get_inpainted_fname(self, index):
        item = self.json_data[index % len(self.json_data)]
        fname = item[0]
        bbox_info = item[1].copy()

        fname_splitted = fname.split('/')
        simple_name = fname_splitted[-1].split('.')
        simple_name[0] = '_'.join(['inpainted', simple_name[0], str(bbox_info['y']), str(bbox_info['h']), str(bbox_info['x']), str(bbox_info['w'])]) + '.png'
        fname_splitted[-1] = '.'.join(simple_name)
        return simple_name[0]

    def get_data_for_inpainting(self, index, rndgen, is_training=True):
        item = self.json_data[index % len(self.json_data)]
        fname = item[0]
        bbox_info = item[1].copy()
        scene_img = imread(self.rtsd_images_dir + fname)
        if is_training:
            bbox_info['h'] = rndgen.randint(54, 110)
            bbox_info['w'] = rndgen.randint(54, 110)
            bbox_info['y'] = rndgen.randint(0, scene_img.shape[0] - bbox_info['h'])
            bbox_info['x'] = rndgen.randint(0, scene_img.shape[1] - bbox_info['w'])
            all_area, area_mask = self.get_item_by_bbox(bbox_info, scene_img)[:2]
        else:
            koef = rndgen.uniform(1.15, 1.2)
            bbox_info['y'] -= int(bbox_info['h'] * (koef - 1.0) // 2)
            bbox_info['h'] += int(bbox_info['h'] * (koef - 1.0))
            bbox_info['x'] -= int(bbox_info['w'] * (koef - 1.0) // 2)
            bbox_info['w'] += int(bbox_info['w'] * (koef - 1.0))
            
            bbox_info['y'] = max(bbox_info['y'], 0)
            bbox_info['h'] = min(bbox_info['h'], scene_img.shape[0] - bbox_info['y'])
            bbox_info['x'] = max(bbox_info['x'], 0)
            bbox_info['w'] = min(bbox_info['w'], scene_img.shape[1] - bbox_info['x'])
            bbox_info['fname'] = fname

            #context_frac_prev = self.context_frac
            #self.context_frac = rndgen.uniform(0.4, 0.9)
            all_area, area_mask = self.get_item_by_bbox(bbox_info, scene_img)[:2]
            #self.context_frac = context_frac_prev
        area_mask = area_mask[:,:,None]
        return all_area, all_area * (1 - area_mask), area_mask, scene_img, bbox_info
    
    
    def get_data_for_inpainting_distorted(self, index, rndgen):
        item = self.json_data[index % len(self.json_data)]
        fname = item[0]
        bbox_info = item[1].copy()
        scene_img = imread(self.rtsd_images_dir + fname)

        koef = rndgen.uniform(1.15, 1.2)
        bbox_info['y'] -= int(bbox_info['h'] * (koef - 1.0) // 2)
        bbox_info['h'] += int(bbox_info['h'] * (koef - 1.0))
        bbox_info['x'] -= int(bbox_info['w'] * (koef - 1.0) // 2)
        bbox_info['w'] += int(bbox_info['w'] * (koef - 1.0))

        bbox_info['y'] = max(bbox_info['y'], 0)
        bbox_info['h'] = min(bbox_info['h'], scene_img.shape[0] - bbox_info['y'])
        bbox_info['x'] = max(bbox_info['x'], 0)
        bbox_info['w'] = min(bbox_info['w'], scene_img.shape[1] - bbox_info['x'])
        bbox_info['fname'] = fname
        
        
        min_y = max(0, bbox_info['y'] - int(rndgen.uniform(0.001, 0.3) * scene_img.shape[0]))
        max_y = min(scene_img.shape[0] - bbox_info['h'] - bbox_info['y'], bbox_info['y'] + int(rndgen.uniform(0.001, 0.3) * scene_img.shape[0]))
        min_x = max(0, bbox_info['x'] - int(rndgen.uniform(0.001, 0.3) * scene_img.shape[1]))
        max_x = min(scene_img.shape[1] - bbox_info['w'] - bbox_info['x'], bbox_info['x'] + int(rndgen.uniform(0.001, 0.3) * scene_img.shape[1]))
        distorted_bbox_info = bbox_info.copy()
        distorted_bbox_info['y'] = rndgen.randrange(min_y, max_y)
        distorted_bbox_info['x'] = rndgen.randrange(min_x, max_x)

        all_area, area_mask = self.get_item_by_bbox(bbox_info, scene_img)[:2]
        distorted_all_area, distorted_area_mask = self.get_item_by_bbox(distorted_bbox_info, scene_img)[:2]
        
        area_mask = area_mask[:,:,None]
        distorted_area_mask = distorted_area_mask[:,:,None]
        return all_area, all_area * (1 - area_mask), area_mask, scene_img, bbox_info, distorted_all_area, distorted_all_area * (1 - distorted_area_mask), distorted_area_mask, distorted_bbox_info
    
    def get_real_sign_image(self, index, rndgen, is_training=True):
        index += rndgen.randint(0, 100000)
        item = self.json_data[index % len(self.json_data)]
        fname = item[0]
        bbox_info = item[1].copy()
        scene_img = imread(self.rtsd_images_dir + fname)
        
        koef = rndgen.uniform(1.15, 1.2)
        bbox_info['y'] -= int(bbox_info['h'] * (koef - 1.0) // 2)
        bbox_info['h'] += int(bbox_info['h'] * (koef - 1.0))
        bbox_info['x'] -= int(bbox_info['w'] * (koef - 1.0) // 2)
        bbox_info['w'] += int(bbox_info['w'] * (koef - 1.0))

        bbox_info['y'] = max(bbox_info['y'], 0)
        bbox_info['h'] = min(bbox_info['h'], scene_img.shape[0] - bbox_info['y'])
        bbox_info['x'] = max(bbox_info['x'], 0)
        bbox_info['w'] = min(bbox_info['w'], scene_img.shape[1] - bbox_info['x'])
        
        real_sign, real_mask, real_class = self.get_item_by_bbox(bbox_info, scene_img)[:3]
        real_class = '.'.join(real_class.split('_'))
        return real_sign, real_mask, real_class


class Dataset(torch.utils.data.Dataset):
    def __init__(self, config, training=True):
        super(Dataset, self).__init__()
        self.input_size = config.INPUT_SIZE
        #self.icons_dataset = IconsImages('/mydata/kursovaya4/style-based-gan-pytorch/data/icons/')
        self.icons_dataset = IconsImages('./china_icons/')
        self.road_images = RtsdRoadImages(resolution=self.input_size, training=training)
        self.is_first = True        
        self.training = training

    def __len__(self):
        return self.road_images.get_length()

    def __getitem__(self, index):
        try:
            item = self.load_item(index)
        except:
            print('loading error: ' + str(index))
            item = self.load_item(index)

        return item

    def load_name(self, index):
        return self.road_images.get_inpainted_fname(index)

    def load_item(self, index):
        if self.is_first:
            import random
            self.rnd = random.Random(index)
            self.is_first = False    
        #from skimage.filters import gaussian
        all_area, area_masked, area_mask, scene_img, bbox_info = self.road_images.get_data_for_inpainting(index, self.rnd, self.training)
        area_mask = (area_mask != 0).astype(np.float32)
        area_mask = np.maximum(area_mask, gaussian(area_mask, sigma=3))
        cls = self.rnd.randint(0, 6)
        icon_img, icon_mask = self.icons_dataset.get_placed_icon(cls, self.input_size // 2, self.rnd, is_test=True)
        bbox_info['sign_class'] = '_'.join(self.icons_dataset.get_int_to_classes(cls).split('.'))

        real_sign_img, real_sign_mask, real_class = self.road_images.get_real_sign_image(index, self.rnd, self.training)
        real_icon_img, real_icon_mask = self.icons_dataset.get_placed_icon(self.icons_dataset.get_classes_to_int(real_class), self.input_size // 2, self.rnd, is_test=True)

        if self.training:
            return self.to_tensor(all_area), self.to_tensor(area_masked), self.to_tensor(area_mask), self.to_tensor(icon_img), self.to_tensor(icon_mask), self.to_tensor(real_sign_img), self.to_tensor(real_sign_mask), self.to_tensor(real_icon_img), self.to_tensor(real_icon_mask)
        else:
            return self.to_tensor(all_area), self.to_tensor(area_masked), self.to_tensor(area_mask), self.to_tensor(icon_img), self.to_tensor(icon_mask), self.to_tensor(real_sign_img), self.to_tensor(real_sign_mask), self.to_tensor(real_icon_img), self.to_tensor(real_icon_mask), self.to_tensor(scene_img), bbox_info

    def to_tensor(self, img):
        #simg = Image.fromarray(img)
        img_t = F.to_tensor(img).float()
        return img_t

    def create_iterator(self, batch_size):
        while True:
            sample_loader = DataLoader(
                dataset=self,
                batch_size=batch_size,
                drop_last=True,
                shuffle=True
            )

            for item in sample_loader:
                yield item

                

class DatasetNumpy(object):
    def __init__(self, config, training=True):
        self.input_size = config.INPUT_SIZE
        #self.icons_dataset = IconsImages('/mydata/kursovaya4/style-based-gan-pytorch/data/icons/')
        self.icons_dataset = IconsImages('./china_icons/')
        self.road_images = RtsdRoadImages(resolution=self.input_size, classes=self.icons_dataset.classes, training=training)
        self.is_first = True        
        self.training = training
        self.is_distort_sign = True

    def __len__(self):
        return self.road_images.get_length()

    def __getitem__(self, index):
        try:
            item = self.load_item(index)
        except:
            print('loading error: ' + str(index))
            item = self.load_item(index)

        return item

    def load_name(self, index):
        return self.road_images.get_inpainted_fname(index)

    def load_item(self, index):
        if self.is_first:
            import random
            self.rnd = random.Random(index)
            self.is_first = False    
        #from skimage.filters import gaussian
        cls = self.rnd.randint(0, 7)
        all_area, area_masked, area_mask, scene_img, bbox_info = self.road_images.get_data_for_inpainting(index, self.rnd, self.training)
        area_mask = (area_mask != 0).astype(np.float32)
        area_mask = np.maximum(area_mask, gaussian(area_mask, sigma=3))
        
        icon_img, icon_mask = self.icons_dataset.get_placed_icon(cls, self.input_size // 2, self.rnd, is_test=True, is_distort_sign=self.is_distort_sign)
        bbox_info['sign_class'] = '_'.join(self.icons_dataset.get_int_to_classes(cls).split('.'))

        real_sign_img, real_sign_mask, real_class = self.road_images.get_real_sign_image(index, self.rnd, self.training)
        real_icon_img, real_icon_mask = self.icons_dataset.get_placed_icon(self.icons_dataset.get_classes_to_int(real_class), self.input_size // 2, self.rnd, is_test=True)

        return self.to_tensor(all_area), self.to_tensor(area_masked), self.to_tensor(area_mask), self.to_tensor(icon_img), self.to_tensor(icon_mask), self.to_tensor(real_sign_img), self.to_tensor(real_sign_mask), self.to_tensor(real_icon_img), self.to_tensor(real_icon_mask), self.to_tensor(scene_img), bbox_info

    def to_tensor(self, img):
        return img.astype(np.float32)

    def create_iterator(self, batch_size):
        while True:
            sample_loader = DataLoader(
                dataset=self,
                batch_size=batch_size,
                drop_last=True
            )

            for item in sample_loader:
                yield item

                
                

class DatasetNumpyDistorted(object):
    def __init__(self, config, training=True):
        self.input_size = config.INPUT_SIZE
        self.icons_dataset = IconsImages('/mydata/kursovaya4/style-based-gan-pytorch/data/icons/')
        self.road_images = RtsdRoadImages(resolution=self.input_size, classes=self.icons_dataset.classes, training=training)
        self.is_first = True        
        self.training = training

    def __len__(self):
        return self.road_images.get_length()

    def __getitem__(self, index):
        try:
            item = self.load_item(index)
        except:
            print('loading error: ' + str(index))
            item = self.load_item(index)

        return item

    def load_name(self, index):
        return self.road_images.get_inpainted_fname(index)

    def load_item(self, index, cls):
        if self.is_first:
            import random
            self.rnd = random.Random(index)
            self.is_first = False
        #from skimage.filters import gaussian
        all_area, area_masked, area_mask, scene_img, bbox_info, distorted_all_area, distorted_area_masked, distorted_area_mask, distorted_bbox_info = self.road_images.get_data_for_inpainting_distorted(index, self.rnd, self.training)
        area_mask = (area_mask != 0).astype(np.float32)
        area_mask = np.maximum(area_mask, gaussian(area_mask, sigma=3))
        distorted_area_mask = (distorted_area_mask != 0).astype(np.float32)
        distorted_area_mask = np.maximum(distorted_area_mask, gaussian(distorted_area_mask, sigma=3))
        
        icon_img, icon_mask = self.icons_dataset.get_placed_icon(cls, self.input_size // 2, self.rnd, is_test=True)
        distorted_bbox_info['sign_class'] = '_'.join(self.icons_dataset.get_int_to_classes(cls).split('.'))

        return self.to_tensor(area_masked), self.to_tensor(area_mask), self.to_tensor(icon_img), self.to_tensor(icon_mask), self.to_tensor(scene_img), bbox_info, self.to_tensor(distorted_area_masked), self.to_tensor(distorted_area_mask), distorted_bbox_info

    def to_tensor(self, img):
        return img.astype(np.float32)

    def create_iterator(self, batch_size):
        while True:
            sample_loader = DataLoader(
                dataset=self,
                batch_size=batch_size,
                drop_last=True
            )

            for item in sample_loader:
                yield item
