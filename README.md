It is a modification of [EdgeConnect](https://github.com/knazeri/edge-connect)

Train:
```
python train.py
```

Generate data:
```
python test.py
```
There are implemented a few generation scripts in `src/edge_connect.py`. They are all called as `test_*()`. The best option is `test_newdataset_faster()`.
